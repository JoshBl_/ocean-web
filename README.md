## Welcome to Ocean! 

Thanks for looking at Ocean - a free, open source, web application that makes travelling around London easier. This application was built using HTMl, CSS, JavaScript, jquery and TypeScript.

This README goes into detail on how to use the application and general information.

## General Use

The application allows you to do the following:
- View Underground line status
- Get directions from one Underground station to another Underground station.
   - This will also display the current cost of the journey.
   - Displays the fare.
   - The travel time.
   - Detailed steps.
   - Disruptions for each step (if there are any disruptions).

## Use
The application can be accessed here - https://limitless-ravine-34034.herokuapp.com/index.html

## Bug reporting

As this software is in active development (and in early access somewhat), there will be bugs. If you find any, please let me know by dropping me an email.


## About

Ocean is written by Joshua Blewitt - who is an aspiring junior developer. Why not check out my [website](https://joshblewitt.dev/) for contact information and get in touch?
