function routeChecker(start, end) {
    console.log('Getting route between ' + start + ' and ' + end);
    var url = "https://api.tfl.gov.uk/journey/journeyresults/" + start + "/to/" + end + "&app_id=028495b7&app_key=bd7089722802f245d2d183c4be394ef8";
    console.log("URL is " + url);
    // this GET request will return 300 multiple choice
    // new object for XMLHttpRequest
    var xmlhttp = new XMLHttpRequest();
    // open method called; verb, url and not asychronous (browser will wait for the call to finish before continuing)
    xmlhttp.open('GET', url, false);
    xmlhttp.send();
    // log info to console
    console.log(xmlhttp);
    console.log(xmlhttp.status);
    if (xmlhttp.status === 300) {
        // variable stores the response from GET request
        var json = xmlhttp.responseText;
        // decode the data stored in responseText - turn it into a native JS object
        var icsCodes = JSON.parse(json);
        // call function with passing object
        icsCodeFetch(icsCodes);
    }
    else {
        console.log('Error! Please try again!');
        $('.journeyError').append('<p>Error! Please try again!</p>');
    }
}
function icsCodeFetch(data) {
    console.log('Fetching ICS codes');
    // create new variables and assign them the ics codes from the response
    var start = data.fromLocationDisambiguation.disambiguationOptions[0].parameterValue;
    var end = data.toLocationDisambiguation.disambiguationOptions[0].parameterValue;
    // print values in variables to console
    console.log('ICS code for start point is ' + start);
    console.log('ICS code for end point is ' + end);
    // call function passing the ics codes
    getJourney(start, end);
}
function getJourney(start, end) {
    // build the URL needed using the ics codes
    var url = 'https://api.tfl.gov.uk/journey/journeyresults/' + start + '/to/' + end + "&app_id=028495b7&app_key=bd7089722802f245d2d183c4be394ef8";
    // log info to console
    console.log('Grabbing route!');
    console.log('URL is ' + url);
    // ajax to perform get request
    $.ajax({
        url: url
    }).done(function (data) {
        // call function
        displayJourney(data);
    }).fail(function () {
        // log to console
        console.log('Error! Unable to find routes!');
        // display message on screen
        $('.journeyError').append('<p>Error! Please try again!</p>');
    });
}
function displayJourney(data) {
    // object containing all steps of a journey
    var steps = data.journeys[0].legs;
    // object containing journey data
    var step = data.journeys[0];
    // displaying journey duration
    console.log('Duration will be ' + data.journeys[0].duration + ' minutes');
    $('.duration').append('<p>Duration will be ' + data.journeys[0].duration + ' minutes</p>');
    // displaying journey price
    console.log('Total price for the journey will be ' + data.journeys[0].fare.fares[0].cost);
    // assigning variable the fare
    var price = data.journeys[0].fare.fares[0].cost;
    // dividing value by 100
    price = price / 100;
    // fixing 2 decimal places to display accurate price
    price = price.toFixed(2);
    // display the total price using UTF-8 to display currency symbol
    $('.pricing').append('<p>Total price for the journey will be &#8356;' + price + '</p>');
    // counter variable
    var i = 0;
    // iterate through the steps object
    $(steps).each(function () {
        // checking value stored in the isDisrupted property
        console.log('isDisrupted has returned ' + data.journeys[0].legs[i].isDisrupted);
        // display the instruction summary
        console.log(step.legs[i].instruction.summary);
        // print the instruction summary
        $('.step').append('<p>' + step.legs[i].instruction.summary + '</p>');
        // new object variable which holds the descriptions for a step
        var details = step.legs[i].instruction.steps;
        // iterate through each element in details
        details.forEach(function (element) {
            // display the description
            console.log(element.description);
            // print the description heading property and the description property
            $('.step').append('<ul><li class="detail">' + element.descriptionHeading + element.description + '</li></ul>');
        });
        // if isDisrupted returns true, run the following
        if (data.journeys[0].legs[i].isDisrupted === true) {
            console.log('Disruption detected on step!');
            // find data and assign to variable
            var disruptionData = step.legs[i].disruptions[0].description;
            // remove any new line (\n) breaks on a global level
            var disruptionDescription = disruptionData.replace(/\\n/g, "");
            // display information
            console.log(disruptionDescription);
            $('.step').append('<div class="important"><p class="text">Disruption Alert!</p><p class="text">' + disruptionDescription + '</p></div>');
        }
        // increment the counter by 1
        i = i + 1;
    });
}
